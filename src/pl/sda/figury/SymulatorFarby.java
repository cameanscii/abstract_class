package pl.sda.figury;

public class SymulatorFarby {

    public static int obliczZapotrzebowanieNaFarbe(Figura[] ksztalty,double pojemnosc){


        int zapotrzebowanie =0;
        double sumaPol=0;
        for(Figura ksztalt:ksztalty){
            sumaPol+=ksztalt.obliczPole();
        }
        zapotrzebowanie=(int)(sumaPol/pojemnosc);

        if (sumaPol%pojemnosc!=0){
            zapotrzebowanie+=1;
        }

        return zapotrzebowanie;
    }

    public static void main(String[] args) {

        Okreg okreg=new Okreg(5);
        Kwadrat kwadrat = new Kwadrat(4);
        Prostokat prostokat = new Prostokat(6,10);
        Figura[] figury = new Figura[]{okreg,kwadrat,prostokat};


        int x =obliczZapotrzebowanieNaFarbe(figury,5);
        System.out.println(x);




    }

}
