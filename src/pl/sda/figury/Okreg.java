package pl.sda.figury;

public class Okreg extends Figura{


    protected final static double PI =3.14;
    protected double r;

    public Okreg(double r) {
        this.r = r;
    }

    @Override
    public double obliczObwod() {
        return 2*PI*r;
    }

    @Override
    public double obliczPole() {
        return PI*r*r;
    }
}
